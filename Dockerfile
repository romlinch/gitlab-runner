FROM gitlab/gitlab-runner:ubuntu
RUN apt-get update && apt-get install --no-install-recommends  -y \
      curl \
      tar \
      unzip \
    && rm -rf /var/lib/apt/lists/*


RUN curl -SL "https://services.gradle.org/distributions/gradle-7.3.3-bin.zip" -o /tmp/gradle.zip \
    && unzip -d /opt /tmp/gradle.zip \
    && rm -rf /tmp/gradle.zip


RUN curl -SL "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.3.0/graalvm-ce-java11-linux-amd64-21.3.0.tar.gz" -o /tmp/graalvm.tar.gz \
  && tar -xzf /tmp/graalvm.tar.gz -C /opt \
  && rm -rf /tmp/graalvm.tar.gz

ENV PATH=/opt/gradle-7.3.3/bin:/opt/graalvm-ce-java11-21.3.0/bin:$PATH
ENV JAVA_HOME=/opt/graalvm-ce-java11-21.3.0
